﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDownShooter
{
    class Flag
    {
        Team team;
        Vector2 position;
        Vector2 startPosition;

        bool isTaken;

        public Flag(Team team, Vector2 pos)
        {
            this.team = team;
            this.position = pos;
        }
    }
}
