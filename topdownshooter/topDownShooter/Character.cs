﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDownShooter
{
    public class Character
    {
        static Random rand = new Random();

        public Texture2D texture;
        public Vector2 pos;
        public Color color;
        public Vector2 origin;
        Vector2 holder;
        SoundEffect fireNoise;
        SoundEffect outOfAmmo;

        public Team team;

        float rotationAngle;
        public bool isShooting;
        int fireSpeed = 100;
        int shotTimer;
        public int totalAmmo = 800;
        public int currentAmmo = 20;
        public int health = 100;
        public Rectangle box;

        int reloadTime;
        int totalReloadTime = 1000;

        int respawnTime = 3000;
        int respawnTimer;

        public bool isDead = false;

        public bool isReloading = false;

        int maxStam = 100;
        int stamina = 100;

        bool isSprinting;

        bool collidingW, collidingS, collidingA, collidingD;

        static int getRand(int one, int two)
        {
            int r = rand.Next(one, two);

            return r;
        }

        public void loadContent(ContentManager Content)
        {
            fireNoise = Content.Load<SoundEffect>("shoot");
            outOfAmmo = Content.Load<SoundEffect>("out");
        }

        public Character(Team team, Texture2D texture, Vector2 pos)
        {
            this.team = team;
            this.texture = texture;
            this.pos = pos;
            this.color = team.color;
            origin.X = texture.Width / 2;
            origin.Y = texture.Height / 2;

            this.isShooting = false;

            collidingW = false;
            collidingS = false;
            collidingA = false;
            collidingD = false;
        }

        public void Respawn(GameTime gameTime)
        {
            respawnTimer += gameTime.ElapsedGameTime.Milliseconds;

            this.isShooting = false;

            if (respawnTimer > respawnTime)
            {
                respawnTimer = 0;
                this.health = 100;
                this.isDead = false;
            }
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            MouseState mState = Mouse.GetState();

            box = new Rectangle((int)this.pos.X, (int)this.pos.Y, this.texture.Width, this.texture.Height);

            holder = new Vector2(mState.X - pos.X, mState.Y - pos.Y);
            holder.Normalize();

            rotationAngle = (float)Math.Atan2(holder.Y, holder.X);

            float offSet = getRand(-1, 2);
            offSet /= 5.0f;

            #region moving

            if (state.IsKeyDown(Keys.LeftShift))
            {
                if(stamina > 0)
                    isSprinting = true;

                if (stamina <= 0)
                    isSprinting = false;
            }

            else
            {
                isSprinting = false;
                if (stamina < maxStam)
                    stamina++;
            }

            foreach (Tile t in TileManager.tiles)
            {
                collidingW = t.collidingBottom(this.box) ? true : false;
                collidingS = t.collidingTop(this.box) ? true : false;
                collidingD = t.collidingRight(this.box) ? true : false;
                collidingA = t.collidingLeft(this.box) ? true : false;
            }

            if (state.IsKeyDown(Keys.W))
            {
                if (!collidingW)
                {
                    if (isSprinting)
                    {
                        pos.Y -= 4;
                        stamina--;
                    }
                    else
                    {
                        pos.Y -= 2;
                    }
                }
                else { Console.WriteLine("W Blocked"); }
            }
            if (state.IsKeyDown(Keys.A))
            {
                if (!collidingA)
                {
                    if (isSprinting)
                    {
                        pos.X -= 4;
                        stamina--;
                    }
                    else
                    {
                        pos.X -= 2;
                    }
                }
                else { Console.WriteLine("A Blocked"); }
            }
            if (state.IsKeyDown(Keys.S))
            {
                if (!collidingS)
                {
                    if (isSprinting)
                    {
                        pos.Y += 4;
                        stamina--;
                    }
                    else
                    {
                        pos.Y += 2;
                    }
                }
                else { Console.WriteLine("S Blocked"); }
            }
            if (state.IsKeyDown(Keys.D))
            {
                if (!collidingD)
                {
                    if (isSprinting)
                    {
                        pos.X += 4;
                        stamina--;
                    }
                    else
                    {
                        pos.X += 2;
                    }
                }
                else { Console.WriteLine("D Blocked"); }
            }
            
            //Console.WriteLine(stamina);

            #endregion

            if (state.IsKeyDown(Keys.R))
            {
                isReloading = true;
            }

            if (isReloading == false)
            {
                if (isShooting)
                {
                    shotTimer += gameTime.ElapsedGameTime.Milliseconds;

                    if (shotTimer > fireSpeed)
                    {
                        
                        shotTimer = 0;

                        offSet = 0;

                        EntityManager.AddBullet(this.pos, new Vector2(this.holder.X + offSet, this.holder.Y + offSet), 12, 2000, team);
                        currentAmmo -= 1;

                        if (currentAmmo <= 0)
                            isReloading = true;
                    }
                }
            }    
            else
            {
                reloadTime += gameTime.ElapsedGameTime.Milliseconds;

                if (reloadTime > totalReloadTime)
                {
                    if (totalAmmo > 0)
                    {
                        int reloadAmount;
                        if (totalAmmo <= 20 && !(totalAmmo + currentAmmo > 20))
                        {
                            reloadAmount = totalAmmo;
                        }
                        else
                        {
                            reloadAmount = 20 - currentAmmo;
                        }
                        
                        reloadTime = 0;
                        currentAmmo += reloadAmount;
                        totalAmmo -= reloadAmount;
                        isReloading = false;

                        if (totalAmmo < 0)
                            totalAmmo = 0;
                    }
                    else
                    {

                    }
                }
            }          
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, pos, null, this.color, rotationAngle, origin, 1.0f, SpriteEffects.None, 1.0f);
        }
    }
}