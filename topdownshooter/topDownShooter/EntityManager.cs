﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDownShooter
{
    public class EntityManager : DrawableGameComponent
    {
        public List<Character> players = new List<Character>();
        static List<Bullet> bullets = new List<Bullet>();
        static List<int> toBeRemoved = new List<int>();

        static Texture2D bulletTexture;

        SpriteBatch spriteBatch;

        Rectangle mapBounds;

        public EntityManager(Game game, SpriteBatch spritebatch, List<Character> players)
            : base(game)
        {
            game.Components.Add(this);
            
            bulletTexture = game.Content.Load<Texture2D>("bullet");

            mapBounds = new Rectangle(0, 0, game.GraphicsDevice.Viewport.Width, game.GraphicsDevice.Viewport.Height);

            this.spriteBatch = spritebatch;

            this.players = players;
        }

        public static void AddBullet(Vector2 pos, Vector2 direction, float speed, int activeTime, Team team)
        {
            Bullet b = new Bullet(bulletTexture, pos, direction, speed, activeTime, team);
            bullets.Add(b);
        }

        public override void Update(GameTime gameTime)
        {
            #region Bullets
            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].Update(gameTime);

                if (bullets[i].totalActiveTime > bullets[i].activeTime)
                {
                    toBeRemoved.Add(i);
                }

                if (mapBounds.Intersects(bullets[i].box) == false)
                {
                    toBeRemoved.Add(i);
                }

                foreach (Character c in players)
                {
                    if ((c.box.Intersects(bullets[i].box)) && (c.team !=bullets[i].team) && (c.isDead == false))
                    {
                        c.health -= 10;
                        toBeRemoved.Add(i);
                    }
                }
                

                for (int it = 0; it < toBeRemoved.Count; it++)
                {
                    if (bullets[i] != null)
                    {
                        bullets.RemoveAt(toBeRemoved[it]);
                        toBeRemoved.RemoveAt(it);
                    }
                }
            }
            #endregion
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (Bullet b in bullets)
            {
                b.Draw(spriteBatch);
            }

            base.Draw(gameTime);
        }

    }
}
