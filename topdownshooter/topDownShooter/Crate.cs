﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDownShooter
{
    enum CrateType {Health, Ammo};

    class Crate
    {
        Vector2 pos;
        Texture2D texture;
        Rectangle rect;
        CrateType cType;

        public Crate(Vector2 pos, Texture2D texture, Rectangle rect, CrateType cType)
        {
            this.pos = pos;
            this.texture = texture;
            this.rect = rect;
            this.cType = cType;
        }

        public bool isColliding(Rectangle rect)
        {
            if (this.rect.Intersects(rect) || rect.Intersects(this.rect))
                return true;

            return false;
        }

        public void Update(GameTime gameTime)
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rect, Color.White);
        }
    }
}
