using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace topDownShooter
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Team Red;
        Team Blue;

        SpriteFont font;

        public List<Character> players;

        List<Crate> crates = new List<Crate>();
        List<int> cToRemove = new List<int>();

        public Rectangle mapBounds;

        Camera view;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.IsMouseVisible = true;


        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            view = new Camera(GraphicsDevice.Viewport);
            base.Initialize();
        }

        protected override void LoadContent()
        {
            players = new List<Character>();

            mapBounds = new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

            Red = new Team(Color.SlateGray);
            Blue = new Team(Color.Blue);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Texture2D bulletTexture = Content.Load<Texture2D>("bullet");
            Texture2D texture = Content.Load<Texture2D>("character");
            font = Content.Load<SpriteFont>("font");

            players.Add(new Character(Red, texture, new Vector2(100,100)));
            //players.Add(new Character(Blue, texture, new Vector2(100, 100)));

            EntityManager entityManager = new EntityManager(this, spriteBatch, players);

            Texture2D tiles = Content.Load<Texture2D>("tiles");
            MapParser.setUpMap("content\\level.txt", tiles);
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            MouseState state = Mouse.GetState();

            #region shooting
            if (state.LeftButton == ButtonState.Pressed)
            {
                foreach (Character c in players)
                {
                    c.isShooting = true;
                }
            }
            else
            {

                foreach (Character c in players)
                {
                    c.isShooting = false;
                }
            }
            #endregion

            #region update and death
            foreach (Character c in players)
            {
                for (int i = 0; i< crates.Count; i++)
                {
                    if (crates[i].isColliding(c.box))
                    {
                        cToRemove.Add(i);
                        c.health += 20;
                    }
                }

                if (c.isDead == false)
                {
                    c.Update(gameTime);
                    
                }

                if (c.isDead == true)
                {
                    c.Respawn(gameTime);
                    c.isShooting = false;
                    //Console.WriteLine("Update:" + c.isDead);
                }

                if (c.health <= 0)
                {
                    c.isDead = true;
                    c.health = 100;
                    break;
                }
            }

            for (int it = 0; it < cToRemove.Count; it++)
            {
                if (crates[it] != null)
                {
                    crates.RemoveAt(cToRemove[it]);
                    cToRemove.RemoveAt(it);
                }
            }

            #endregion

            view.Update(gameTime, this);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Crimson);

            //spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, view.transform);
            spriteBatch.Begin();
            TileManager.draw(spriteBatch);
            #region drawplayers
            foreach (Character c in players)
            {
                if (c.isReloading == true)
                {
                    if (c.totalAmmo + c.currentAmmo == 0)
                    {
                        spriteBatch.DrawString(font, "Out Of Ammo", new Vector2(0, 30), Color.White);
                    }
                    else
                    {
                        spriteBatch.DrawString(font, "Reloading", new Vector2(0, 30), Color.White);
                    }
                    
                }
                if(c.isDead == false)
                    c.Draw(spriteBatch);
                spriteBatch.DrawString(font, c.currentAmmo + "/" + c.totalAmmo, Vector2.Zero, Color.White);
                spriteBatch.DrawString(font, "Health:" + c.health, new Vector2(0 , graphics.PreferredBackBufferHeight - 40), Color.White);
            }
            #endregion

            #region drawCrates

            foreach (Crate c in crates)
            {
                c.Draw(spriteBatch);
            }

            #endregion
            base.Draw(gameTime);
            spriteBatch.End();
            
        }
    }
}
