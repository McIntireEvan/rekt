﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace topDownShooter
{
    public class Bullet
    {
        public Texture2D texture;
        public Vector2 pos;
        public Vector2 direction;
        public float speed;
        public int activeTime;
        public int totalActiveTime;

        public Team team;

        public int damage = 5;

        public Rectangle box;

        public Bullet(Texture2D texture, Vector2 pos, Vector2 direction, float speed, int activeTime, Team team)
        {
            this.team = team;
            this.texture = texture;
            this.pos = pos;
            this.direction = direction;
            this.speed = speed;
            this.activeTime = activeTime;

            this.totalActiveTime = 0;
        }

        public void Update(GameTime gameTime)
        {
            box = new Rectangle((int)this.pos.X, (int)this.pos.Y, this.texture.Width, this.texture.Height);

            this.pos += direction * speed;

            this.totalActiveTime += gameTime.ElapsedGameTime.Milliseconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, pos, null, Color.White, 0f, new Vector2(texture.Width / 2, texture.Height / 2), 1.0f, SpriteEffects.None, 1.0f);
        }
    }
}
