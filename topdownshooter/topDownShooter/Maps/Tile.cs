﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.IO;

namespace topDownShooter
{
    public enum Type { RED, BLUE, GREEN, YELLOW };

    public class Tile
    {
        int indexX;
        int indexY;
        int height = 16;
        int width = 16;
        Type type;

        Texture2D spritesheet;
        public Rectangle rect; 

        public Tile(Texture2D spritesheet, Type type, int indexX, int indexY)
        {
            this.indexX = indexX;
            this.indexY = indexY;
            this.type = type;
            this.spritesheet = spritesheet;
            rect = new Rectangle(indexX * width, indexY * height, width, height);
        }

        public void update(GameTime gameTime)
        {

        }

        public void draw(SpriteBatch spriteBatch)
        {
            int index = 0;
            if(type == Type.RED)
            {
                index = 0;
            }
            if (type == Type.YELLOW)
            {
                index = 2;
            }
            if (type == Type.GREEN)
            {
                index = 3;
            }
            if (type == Type.BLUE)
            {
                index = 1;
            }
            Rectangle sourcerectangle = new Rectangle(index * 16, 0, 16, 16); //Get the Source Rectange from the Spritesheet
            Rectangle destinationRectangle = new Rectangle((indexX * width), indexY * height, width, height);

            spriteBatch.Draw(spritesheet, destinationRectangle, sourcerectangle, Color.White);
        }

        public bool collidingTop(Rectangle rect)
        {
            if ((rect.Intersects(this.rect)) && rect.Bottom - 1 == this.rect.Top)
            {
                Console.WriteLine("Top");
                return true;
            }
            return false;
        }
        public bool collidingRight(Rectangle rect)
        {
            if ((rect.Intersects(this.rect)) && rect.Left + 1 == this.rect.Right)
            {
                Console.WriteLine("Right");
                return true;
            }
            return false;
        }
        public bool collidingLeft(Rectangle rect)
        {
            if ((rect.Intersects(this.rect)) && rect.Right - 1 == this.rect.Left)
            {
                Console.WriteLine("Left");
                return true;
            }
            return false;
        }
        public bool collidingBottom(Rectangle rect)
        {
            if ((rect.Intersects(this.rect)) && rect.Top + 1 == this.rect.Bottom)
            {
                Console.WriteLine("Bottom");
                return true;
            }
            return false;
        }
    }
}
