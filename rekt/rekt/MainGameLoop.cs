﻿#region Using Statements
using FusionEngine.Core;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using REKT.Menus;
#endregion

namespace REKT
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainGameLoop : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        MainMenuScreen menu;
        public static SpriteFont font;
        public static Color background;
        public static Texture2D logo;
        public static Song title;

        public static bool shouldExit = false;

        public MainGameLoop()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
        }

        protected override void Initialize()
        {
            this.Window.AllowUserResizing = false;
            this.graphics.PreferredBackBufferWidth = 720;
            this.graphics.PreferredBackBufferHeight = 720;
            this.graphics.ApplyChanges();
            //this.IsMouseVisible = true;
            base.Initialize();
            GameServices.AddService<GraphicsDevice>(GraphicsDevice);
            GameServices.AddService<GraphicsDeviceManager>(graphics);
            GameServices.AddService<ContentManager>(Content);

            background = new Color(162, 0, 0, 1);
            
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("font");
            logo = Content.Load<Texture2D>("logo");
            Song s = Content.Load<Song>("audio\\title.wav");
            //MediaPlayer.Play(s);
            MediaPlayer.Volume = .1f;
        }

        protected override void BeginRun()
        {
            base.BeginRun();
            menu = new MainMenuScreen(logo);
            menu.state = ScreenState.Active;
            ScreenHandler.AddScreen(menu);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (shouldExit)
                Exit();
            ScreenHandler.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(background);

            ScreenHandler.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}