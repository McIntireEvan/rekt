﻿using FusionEngine.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Guns;
using System;

namespace REKT.Entities.Bullets
{
    public class Bullet : ICollidableObject
    {
        Rectangle hitBox;
        Texture2D texture;
        Gun parent;
        Vector2 pos;
        Vector2 direction;

        float speed;

        public int totalActiveTime;
        public int currentActiveTime;
        public bool shouldBeRemoved;

        public Bullet(Texture2D texture, Gun parent, Vector2 pos, Vector2 direction, float speed, int totalActiveTime)
        {
            this.texture = texture;
            this.parent = parent;
            this.pos = pos;
            this.direction = direction;
            this.speed = speed;
            this.totalActiveTime = totalActiveTime;
            shouldBeRemoved = false;
            hitBox = new Rectangle((int)pos.X, (int)pos.Y, 5, 5);
        }

        public void Update(GameTime gameTime)
        {
            hitBox = new Rectangle((int)this.pos.X, (int)this.pos.Y, this.texture.Width, this.texture.Height);

            this.pos += direction * speed;
            this.currentActiveTime += gameTime.ElapsedGameTime.Milliseconds;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, pos, Color.White);
            //spriteBatch.Draw(texture, pos, null, Color.White, 0f, new Vector2(texture.Width / 2, texture.Height / 2), 1.0f, SpriteEffects.None, 1.0f);
        }

        public Rectangle GetCollisionBox()
        {
            return this.hitBox;
        }

        public Vector2 GetPosition()
        {
            return this.pos;
        }

        public Vector2 GetDirection()
        {
            return this.direction;
        }

        public void OnCollision(ICollidableObject o) 
        {
            shouldBeRemoved = true;
        }
    }
}
