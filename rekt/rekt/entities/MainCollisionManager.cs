﻿using FusionEngine.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Bullets;
using REKT.Entities.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.Entities
{
    public class MainCollisionManager : CollisionManager
    {
        List<Bullet> bullets = new List<Bullet>();
        List<int> toBeRemoved = new List<int>();

        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].Update(gameTime);

                if (bullets[i].currentActiveTime > bullets[i].totalActiveTime)
                {
                    toBeRemoved.Add(i);
                }

                if (bullets[i].shouldBeRemoved)
                {
                    toBeRemoved.Add(i);
                }

                for (int it = 0; it < toBeRemoved.Count; it++)
                {
                    if (bullets[i] != null)
                    {
                        bullets.RemoveAt(toBeRemoved[it]);
                        toBeRemoved.RemoveAt(it);
                    }
                }
            }
            base.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Bullet b in bullets)
            {
                b.Draw(spriteBatch);
            }
        }

        public List<Bullet> GetBullets()
        {
            return bullets;
        }

        public void AddBullet(Bullet b)
        {
            bullets.Add(b);
            foreach(KeyValuePair<ICollidableObject, List<ICollidableObject>> pair in this.trackedObjects)
            {
                if (pair.Key is BasePlayer)
                {
                    pair.Value.Add(b);
                }
            }
        }

        public void RemoveBullet(Bullet b)
        {
            bullets.Remove(b);
            foreach (KeyValuePair<ICollidableObject, List<ICollidableObject>> pair in this.trackedObjects)
            {
                if (pair.Key is BasePlayer)
                {
                    pair.Value.Remove(b);
                }
            }
        }
    }
}
