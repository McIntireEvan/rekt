﻿using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.Entities.Guns
{
    public class FullAutoGun : Gun
    {
        public FullAutoGun(BasePlayer parent, Texture2D texture)
            : base(parent, texture)
        {
            this.type = "Automatic";
        }
    }
}
