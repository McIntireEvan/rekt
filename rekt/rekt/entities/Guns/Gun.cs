﻿using FusionEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Bullets;
using REKT.Entities.Characters;
using REKT.MainGame;
using System.Collections.Generic;

namespace REKT.Entities.Guns
{
    public class Gun
    {
        public string type;

        protected BasePlayer parent;
        protected Texture2D texture;
        protected List<Bullet> bullets;

        public int currentClip;
        protected int clipSize;

        public int ammo;
        protected int maxAmmo;

        protected int reloadSpeed;
        protected int reloadTimer;

        protected bool reloadRequired;

        protected int fireSpeed = 150;
        protected int fireTimer = 0;
        protected Texture2D bulletTexture;

        public Gun(BasePlayer parent, Texture2D texture)
        {
            this.parent = parent;
            this.texture = texture;
            this.bullets = new List<Bullet>();
            type = "Gun";
            maxAmmo = 500;
            ammo = 480;
            clipSize = 20;
            currentClip = 20;
            reloadSpeed = 1000;
            reloadTimer = 0;
            reloadRequired = false;
            bulletTexture = GameServices.GetService<ContentManager>().Load<Texture2D>("bullet");
        }

        protected virtual void createBullet()
        {
            Bullet b = new Bullet(bulletTexture, 
                                  this, 
                                  new Vector2(parent.getHitBox().Center.X, parent.getHitBox().Center.Y) - new Vector2(bulletTexture.Width / 2, bulletTexture.Height / 2), 
                                  parent.GetRotationAngle(), 
                                  20, 
                                  1000);

            MainGameScreen.collisionManager.AddBullet(b);
        }

        public virtual void doFiringTimeChecking(GameTime gameTime)
        {
            fireTimer += gameTime.ElapsedGameTime.Milliseconds;
            if (fireTimer > fireSpeed)
                fireTimer = fireSpeed;
        }

        public virtual void Update(GameTime gameTime)
        {
            if (reloadRequired)
            {
                doReload(gameTime);
            }

            doFiringTimeChecking(gameTime);
        }

        protected void doReload(GameTime gameTime)
        {
            if (reloadTimer >= 1000)
            {
                reloadTimer = 0;

                if (ammo != 0)
                {
                    int newClip = (clipSize < ammo) ? clipSize : ammo;

                    currentClip = clipSize;
                    ammo -= newClip;
                }
                reloadRequired = false;
            }
            else
            {
                reloadTimer += gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        protected virtual bool checkConditions(GameTime gameTime)
        {
            if (ammo == 0 && currentClip == 0)
            {
                return false;
            }

            if (currentClip == 0)
            {
                reloadRequired = true;
                return false;
            }

            return true;
        }

        public virtual void onFire(GameTime gameTime)
        {
            if (fireTimer == fireSpeed)
            {
                if (checkConditions(gameTime))
                {
                    createBullet();
                    currentClip -= 1;
                }
                fireTimer = 0;
            }
        }
    }
}