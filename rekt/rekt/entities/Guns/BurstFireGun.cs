﻿using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.Entities.Guns
{
    public class BurstFireGun : Gun
    {
        private int bulletsPerBurst;
        private int currentBullets;

        public BurstFireGun(BasePlayer parent, Texture2D texture) : base(parent, texture)
        {
            this.type = "Burst Fire";
            bulletsPerBurst = 3;
            currentBullets = 3;
            this.fireSpeed = base.fireSpeed * bulletsPerBurst;
        }

        public override void onFire(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (fireTimer == fireSpeed && currentBullets != 0)
            {
                if (checkConditions(gameTime))
                {
                    createBullet();
                    currentBullets--;
                    currentClip--;
                }
                if (currentBullets != 0)
                {
                    fireTimer = fireSpeed - 75;
                }
                else
                {
                    fireTimer = 0;
                    currentBullets = bulletsPerBurst;
                }
                
            }
        }
    }
}