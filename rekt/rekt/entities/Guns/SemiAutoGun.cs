﻿
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using REKT.Entities.Characters;
using System;
namespace REKT.Entities.Guns
{
    public class SemiAutoGun : Gun
    {
        private bool canFire;
        private bool hasFired;

        public SemiAutoGun(BasePlayer parent, Texture2D texture) : base(parent, texture)
        {
            type = "Semi-Automatic";
            canFire = false;
            fireSpeed = 150;
            hasFired = false;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (Mouse.GetState().LeftButton == ButtonState.Released)
            {
                hasFired = false;
            }

            if (hasFired) canFire = false;
            else canFire = true;

            base.Update(gameTime);
        }

        protected override bool checkConditions(Microsoft.Xna.Framework.GameTime gameTime)
        {
            if (!canFire)
            {
                return false;
            }
            
            return base.checkConditions(gameTime);
        }

        public override void onFire(Microsoft.Xna.Framework.GameTime gameTime)
        {
            
            base.onFire(gameTime);
            hasFired = true;
        }
    }
}