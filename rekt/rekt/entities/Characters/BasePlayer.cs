﻿using FusionEngine.Collision;
using FusionEngine.Core;
using FusionEngine.Entities;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using REKT.Entities.Guns;
using REKT.UI.Elements;
using System;

namespace REKT.Entities.Characters
{
    public class BasePlayer : Player, ICollidableObject
    {
        HealthBar h;
        Camera camera;

        protected MouseState currentState;
        protected Gun[] guns;
        protected int currentGunIndex;

        protected int totalHealth = 100;
        protected int currentHealth = 100;

        public BasePlayer(Vector2 pos, Texture2D texture)
            : base(pos, texture, true)
        {
            currentGunIndex = 0;

            guns = new Gun[4];
            guns[0] = new BurstFireGun(this, GameServices.GetService<ContentManager>().Load<Texture2D>("gun"));
            guns[1] = new FullAutoGun(this, GameServices.GetService<ContentManager>().Load<Texture2D>("gun"));
            guns[2] = new SemiAutoGun(this, GameServices.GetService<ContentManager>().Load<Texture2D>("gun"));
            guns[3] = new ShotGun(this, GameServices.GetService<ContentManager>().Load<Texture2D>("gun"));

            h = new HealthBar(Color.Red, this, new Vector2(100, 0), 100);
        }

        public void SetCamera(Camera camera)
        {
            this.camera = camera;
        }

        public int GetHealth()
        {
            return this.currentHealth;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            MouseState m = Mouse.GetState();
            GamePadState state = GamePad.GetState(PlayerIndex.One);

            if (state.DPad.Up == ButtonState.Pressed)
            {
                MoveY(-5);
            }
            if (state.DPad.Down == ButtonState.Pressed)
            {
                MoveY(5);
            }
            if (state.DPad.Left == ButtonState.Pressed)
            {
                MoveX(-5);
            }
            if (state.DPad.Right== ButtonState.Pressed)
            {
                MoveX(5);
            }

            MoveX((state.ThumbSticks.Left.X * 5));
            MoveY(-(state.ThumbSticks.Left.Y * 5));

            float xChange = state.ThumbSticks.Right.X;
            float yChange = state.ThumbSticks.Right.Y;

            int newX = m.X;

            if(state.ThumbSticks.Right.X > .3 || state.ThumbSticks.Right.X < -.3) {
                newX = (int)(m.X + (xChange * 10));
                
            }

            int newY = m.Y;

            if(state.ThumbSticks.Right.Y > .3 || state.ThumbSticks.Right.Y < -.3) {
                newY = (int)(m.Y + (-yChange * 10));
            }

            if (m.X != newX || m.Y != newY)
            {
                Mouse.SetPosition(newX, newY);
            }

            guns[currentGunIndex].Update(gameTime);

            if (m.RightButton == ButtonState.Pressed)
            {
                currentHealth--;
            }

            if ((m.ScrollWheelValue / 120) > (currentState.ScrollWheelValue / 120))
            {
                if (currentGunIndex == guns.Length - 1)
                    currentGunIndex = 0;
                else
                    currentGunIndex++;
            }
            else if ((m.ScrollWheelValue / 120) < (currentState.ScrollWheelValue / 120))
            {
                if (currentGunIndex == 0)
                    currentGunIndex = guns.Length - 1;
                else
                    currentGunIndex--;
            }
            else
            {
                if (m.LeftButton == ButtonState.Pressed || state.Triggers.Right > .5)
                {
                    guns[currentGunIndex].onFire(gameTime);
                }
            }
            currentState = m;

            h.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //h.Draw(spriteBatch);
            
            //TODO: Move to HUD
            //spriteBatch.DrawString(MainGameLoop.font, guns[currentGunIndex].type, Vector2.Zero, Color.Black);
            //spriteBatch.DrawString(MainGameLoop.font, guns[currentGunIndex].currentClip + "/" + guns[currentGunIndex].currentAmmo, new Vector2(0, 100), Color.Black);

            base.Draw(spriteBatch);
        }

        public Rectangle GetCollisionBox()
        {
            return this.getHitBox();
        }

        public Vector2 GetRotationOrigin()
        {
            return this.rotationOrigin;
        }

        public Vector2 GetRotationAngle()
        {
            Vector2 holder = new Vector2(currentState.X, currentState.Y) - new Vector2(this.hitBox.Center.X, this.hitBox.Center.Y);
            holder = Vector2.Transform(holder, Matrix.Invert(camera.getMatrix()));
            holder.Normalize();
            return holder;
        }

        public void OnCollision(ICollidableObject o)
        {

        }
    }
}
