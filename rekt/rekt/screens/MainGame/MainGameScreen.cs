﻿using FusionEngine;
using FusionEngine.Core;
using FusionEngine.Entities;
using FusionEngine.Screens;
using FusionEngine.Shapes;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using REKT.Entities;
using REKT.Entities.Bullets;
using REKT.Entities.Characters;
using REKT.Menus;
using System;
using System.Collections.Generic;

namespace REKT.MainGame
{
    class MainGameScreen : GameScreen
    {
        public static MainCollisionManager collisionManager;
        public static List<Bullet> NewBullets;

        BasePlayer player1;
        
        NetClient client;
        int clientID = -1;
        Dictionary<int, Player> players;

        Texture2D sprite;

        Camera camera;

        public MainGameScreen()
        {
            MainGameLoop.background = Color.White;
            
        }

        public override void LoadContent()
        {
            base.LoadContent();
            sprite = Content.Load<Texture2D>("character");
            player1 = new BasePlayer(new Vector2(100, 100), sprite);
            
            camera = new Camera(GameServices.GetService<GraphicsDevice>().Viewport);
            player1.SetCamera(camera);
            player1.bindKeyToFunction("MoveX", Keys.A, -5);
            player1.bindKeyToFunction("MoveX", Keys.D, 5);
            player1.bindKeyToFunction("MoveY", Keys.W, -5);
            player1.bindKeyToFunction("MoveY", Keys.S, 5);
            collisionManager = new MainCollisionManager();

            connectToServer("dev-server", 14242);

            players = new Dictionary<int, Player>();
            NewBullets = new List<Bullet>();
        }

        public void connectToServer(String name, int port)
        {
            NetPeerConfiguration config = new NetPeerConfiguration(name);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);

            client = new NetClient(config);
            client.Start();
            
            client.Connect("evanmcintire.com",port);
        }

        public override void Update(GameTime gameTime)
        {
            MainGameLoop.background = Color.White;
            KeyboardState k = Keyboard.GetState();
            MouseState m = Mouse.GetState();

            if (k.IsKeyDown(Keys.Escape))
            {
                PauseMenuScreen pM = new PauseMenuScreen(this);
                pM.state = ScreenState.Active;
                ScreenHandler.AddScreen(pM);
                this.state = ScreenState.Hidden;
            }

            ReadFromServer(gameTime);

            WriteToServer();

            player1.Update(gameTime);
            player1.updateRotation(new Vector2(m.X, m.Y), camera);
            collisionManager.Update(gameTime);
            camera.Update(gameTime, player1.getHitBox());

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, null, null, null, null, camera.getMatrix());

            Vector2 crosshairPos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            crosshairPos = Vector2.Transform(crosshairPos, Matrix.Invert(camera.getMatrix()));
            crosshairPos = new Vector2(crosshairPos.X - 13, crosshairPos.Y - 13);
            spriteBatch.Draw(GameServices.GetService<ContentManager>().Load<Texture2D>("crosshair"), crosshairPos, Color.White);

            foreach (int i in players.Keys)
            {
                players[i].Draw(spriteBatch);
            }

            collisionManager.Draw(spriteBatch);
            player1.Draw(spriteBatch);
            spriteBatch.DrawString(MainGameLoop.font, "0,0", Vector2.Zero, Color.Black);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void ReadFromServer(GameTime gameTime)
        {
            NetIncomingMessage msg;

            while ((msg = client.ReadMessage()) != null)  
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryResponse:
                        // just connect to first server discovered
                        client.Connect(msg.SenderEndpoint);
                        break;
                    case NetIncomingMessageType.Data:
                        String type = msg.ReadString();

                        if (type.Equals("PlayerConnect"))
                        {
                            clientID = msg.ReadInt32();
                            Console.WriteLine("Client ID is " + clientID);
                            type = "";
                        }
                        else if (type.Equals("Updates"))
                        {
                            int numMessages = msg.ReadInt32();

                            for (int i = 0; i < numMessages / 3; i++)
                            {
                                int id = msg.ReadInt32();
                                float x = msg.ReadFloat();
                                float y = msg.ReadFloat();
                                float angle = msg.ReadFloat();
                                float rX = msg.ReadFloat();
                                float rY = msg.ReadFloat();
                                if (!players.ContainsKey(id))
                                {
                                    if (id != this.clientID)
                                    {
                                        Console.WriteLine("New Player detected");
                                        Console.WriteLine(x + ":" + y);
                                        Player player = new Player(new Vector2(x, y), sprite, true);
                                        players.Add(id, player);
                                    }
                                }
                                else
                                {
                                    //players[id].Update(gameTime, new Vector2(x, y), new Vector2(rX, rY), angle);
                                }
                            }
                        }
                        else if (type.Equals("PlayerDisconnect"))
                        {
                            int pId = msg.ReadInt32();
                            players.Remove(pId);
                        }
                        break;
                }
            }
        }

        private void WriteToServer()
        {
            NetOutgoingMessage ms = client.CreateMessage();
            ms.Write("PlayerMeta");
            ms.Write(player1.getPosition().X);
            ms.Write(player1.getPosition().Y);
            ms.Write(player1.getRotationAngle2());
            ms.Write(player1.GetRotationOrigin().X);
            ms.Write(player1.GetRotationOrigin().Y);
            client.SendMessage(ms, NetDeliveryMethod.ReliableOrdered);

            NetOutgoingMessage bulletInfo = client.CreateMessage();
            bulletInfo.Write("BulletInfo");
            bulletInfo.Write(NewBullets.ToArray().Length * 4);
            foreach (Bullet b in NewBullets)
            {
                bulletInfo.Write(b.GetPosition().X);
                bulletInfo.Write(b.GetPosition().Y);
                bulletInfo.Write(b.GetDirection().X);
                bulletInfo.Write(b.GetDirection().Y);
            }

            client.SendMessage(bulletInfo, NetDeliveryMethod.ReliableOrdered);
        }
    }
}