﻿using FusionEngine.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.Menus;

namespace REKT.Menus.MainMenu.MainMenuItems
{
    class ExitMenuItem  : BaseMenuItem
    {
        public ExitMenuItem(Menu parent, Rectangle rect)
            : base(parent, rect)
        {
            this.selectionArea = rect;
            this.text = "Exit";
        }

        public override void onActivation()
        {
            base.onActivation();
            MainGameLoop.shouldExit = true;
        }
    }
}
