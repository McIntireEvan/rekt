﻿using FusionEngine.Menu;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.MainGame;
using REKT.Menus;

namespace REKT.Menus.MainMenu.MainMenuItems
{
    public class PlayMenuItem : BaseMenuItem
    {

        MainMenuScreen parent;
        public PlayMenuItem(MainMenuScreen parent, Rectangle rect)
            : base(parent, rect)
        {
            this.selectionArea = rect;
            this.parent = parent;
        }

        public override void onActivation()
        {
            base.onActivation();
            //TODO: Replace with statehandler thingy
            MainGameScreen newScreen = new MainGameScreen();
            newScreen.state = ScreenState.Active;
            ScreenHandler.AddScreen(newScreen);
            parent.state = ScreenState.Off;
        }
    }
}
