﻿using FusionEngine.Core;
using FusionEngine.Menu;
using FusionEngine.Menu.MenuControls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.Menus.MainMenu.MainMenuItems;

namespace REKT.Menus
{
    public class MainMenuScreen : Menu
    {
        MenuImage menuLogo;

        public MainMenuScreen(Texture2D logo)
        {
            GraphicsDeviceManager g = GameServices.GetService<GraphicsDeviceManager>();

            int scale = 30;

            int width = 16 * scale;
            int height = 9 * scale;
            int x = (g.PreferredBackBufferWidth / 2) - (width / 2);
            int y = 0;

            menuLogo = new MenuImage(logo, new Rectangle(x, y, width, height));

            PlayMenuItem play = new PlayMenuItem(this, new Rectangle((g.PreferredBackBufferWidth / 2) - 75, height + 50, 150, 100));
            this.addMenuItem(play);

            ExitMenuItem exit = new ExitMenuItem(this, new Rectangle((g.PreferredBackBufferWidth / 2) - 75, height + 150, 150, 100));
            this.addMenuItem(exit);

            MainGameLoop.background = new Color(162, 0, 0, 1);
        }

        protected override void doCustomUpdate(GameTime gameTime)
        {
        }

        protected override void doCustomDrawing(SpriteBatch spriteBatch)
        {
            menuLogo.Draw(spriteBatch);
        }
    }
}