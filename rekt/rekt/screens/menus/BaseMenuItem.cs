﻿using FusionEngine.Menu;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace REKT.Menus
{
    public class BaseMenuItem : MenuItem
    {
        protected string text = "Play";
        protected Color textColor = Color.White;

        public BaseMenuItem(Menu parent, Rectangle rect)
            : base(parent, rect)
        {
            this.selectionArea = rect;
        }

        public override void whileSelected()
        {
            this.textColor = Color.Gray;
        }

        public override void whileNotSelected()
        {
            this.textColor = Color.White;
        }

        public override void onActivation()
        {
            this.textColor = Color.Black;
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime) { }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(MainGameLoop.font, text, new Vector2(selectionArea.X, selectionArea.Y), textColor);
        }
    }
}
