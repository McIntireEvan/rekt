﻿using FusionEngine.Menu;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using REKT.Menus.PauseMenu.PauseMenuItems;

namespace REKT.Menus
{
    public class PauseMenuScreen : Menu
    {
        public GameScreen parent;

        public PauseMenuScreen(GameScreen parent)
        {
            this.parent = parent;
            
            ReturnMenuItem returnM = new ReturnMenuItem(this, new Rectangle((graphics.PreferredBackBufferWidth / 2) - 187, 50, 375, 100));
            this.addMenuItem(returnM);

            ResumeMenuItem resume = new ResumeMenuItem(this, new Rectangle(graphics.PreferredBackBufferWidth / 2 - 137, 200, 275, 100));
            this.addMenuItem(resume);

            MainGameLoop.background = Color.Firebrick;
        }

        protected override void doCustomDrawing(Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
        }

        protected override void doCustomUpdate(Microsoft.Xna.Framework.GameTime gameTime)
        { 
        }
    }
}
