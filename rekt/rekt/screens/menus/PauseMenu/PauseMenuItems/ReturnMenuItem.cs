﻿using FusionEngine.Menu;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.Menus.PauseMenu.PauseMenuItems
{
    public class ReturnMenuItem : BaseMenuItem
    {
        PauseMenuScreen parent;
        public ReturnMenuItem(PauseMenuScreen parent, Rectangle rect)
            : base(parent, rect)
        {
            this.parent = parent;
            this.text = "Main Menu";

        }
        public override void onActivation()
        {
            base.onActivation();
            MainMenuScreen mainMenu = new MainMenuScreen(MainGameLoop.logo);
            mainMenu.state = ScreenState.Active;
            ScreenHandler.AddScreen(mainMenu);
            parent.parent.state = ScreenState.Off;
            parent.state = ScreenState.Off;
        }
    }
}
