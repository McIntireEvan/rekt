﻿using FusionEngine.Menu;
using FusionEngine.Screens;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.Menus.PauseMenu.PauseMenuItems
{
    public class ResumeMenuItem : BaseMenuItem
    {
        PauseMenuScreen parent;

        public ResumeMenuItem(PauseMenuScreen parent, Rectangle rect)
            : base(parent, rect)
        {
            this.parent = parent;
            this.text = "Resume";
        }

        public override void onActivation()
        {
            base.onActivation();
            parent.parent.state = ScreenState.Active;
            parent.state = ScreenState.Off;
        }
    }
}
