﻿using FusionEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.UI.Generic
{
    public class Background
    {
        private Rectangle rect;
        private Texture2D texture;
        private Color color;

        public Background(Rectangle rect, Color color)
        {
            this.rect = rect;
            this.color = color;
            this.texture = GameServices.GetService<ContentManager>().Load<Texture2D>("white");
        }

        public Background(Texture2D texture, Vector2 pos, Color color)
        {
            this.texture = texture;
            this.rect = new Rectangle((int)pos.X, (int)pos.Y, texture.Width, texture.Height);
            this.color = color;
        }

        public void setRectangle(Rectangle rect)
        {
            this.rect = rect;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rect, color);
        }
    }
}
