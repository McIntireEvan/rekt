﻿using FusionEngine.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.UI.Generic
{
    public abstract class Bar
    {
        public Color color;
        protected Texture2D texture;
        protected BasePlayer player;
        protected Rectangle rectangle;

        public Bar(Color color, BasePlayer player, Rectangle rectangle)
        {
            this.color = color;
            this.player = player;
            this.rectangle = rectangle;

            this.texture = GameServices.GetService<ContentManager>().Load<Texture2D>("white");
        }

        public abstract void getInfo();

        public void Update(GameTime gameTime)
        {
            getInfo();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, color);
        }
    }
}
