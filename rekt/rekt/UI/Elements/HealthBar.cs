﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using REKT.Entities.Characters;
using REKT.UI.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REKT.UI.Elements
{
    public class HealthBar : Bar
    {
        int currentHealth;
        int totalHealth;
        int pixelsPerPoint;
        Background b;

        public HealthBar(Color color, BasePlayer player, Vector2 pos, int pixelsPerPoint) : base(color, player, new Rectangle((int)pos.X, (int)pos.Y, (int)player.GetHealth() * pixelsPerPoint, 50))
        {
            currentHealth = 0;
            totalHealth = 0;
            this.pixelsPerPoint = 5;
            b = new Background(new Rectangle((int)pos.X - 5, (int)pos.Y - 5, (int)(player.GetHealth() * pixelsPerPoint) + 5, 50 + 5), Color.Black);
        }

        public override void getInfo()
        {
            rectangle.Width = (int)(player.GetHealth() * pixelsPerPoint);
        }

        public override void Draw(SpriteBatch spriteBatch) 
        {
            b.Draw(spriteBatch);
            base.Draw(spriteBatch);
        }
    }
}
