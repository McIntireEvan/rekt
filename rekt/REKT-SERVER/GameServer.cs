﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace REKT_SERVER
{
    public class GameServer
    {
        Dictionary<long, int> connections;
        NetPeerConfiguration config;
        NetServer server;
        double nextMessageTime;

        public GameServer(String name, int port)
        {
            connections = new Dictionary<long, int>();
            config = new NetPeerConfiguration(name);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.Port = port;
            server = new NetServer(config);
            server.Start();
            Console.WriteLine("Started server on port " + config.Port);
            nextMessageTime = NetTime.Now;
        }

        public void Update()
        {
            while (!Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape)
            {
                NetIncomingMessage message;

                while ((message = server.ReadMessage()) != null)
                {
                    switch (message.MessageType)
                    {
                        case NetIncomingMessageType.DiscoveryRequest:
                            // Server received a discovery request from a client; send a discovery response (with no extra data attached)
                            server.SendDiscoveryResponse(null, message.SenderEndpoint);
                            Console.WriteLine("New Client Request:" + message.SenderEndpoint);
                            break;
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage:
                            // Just print diagnostic messages to console
                            Console.WriteLine(message.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)message.ReadByte();
                            Console.WriteLine(status);
                            if (status == NetConnectionStatus.Connected)
                            {
                                NetConnection c = message.SenderConnection;
                                Console.WriteLine(NetUtility.ToHexString(c.RemoteUniqueIdentifier) + " connected!");
                                connections.Add(c.RemoteUniqueIdentifier, connections.Keys.ToArray().Length + 1);
                                NetOutgoingMessage m = server.CreateMessage();
                                m.Write("PlayerConnect");
                                m.Write(connections[c.RemoteUniqueIdentifier]);
                                c.SendMessage(m, NetDeliveryMethod.ReliableOrdered, 1);
                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                NetConnection c = message.SenderConnection;
                                Console.WriteLine(NetUtility.ToHexString(c.RemoteUniqueIdentifier) + " disconnected!");
                                
                                foreach (NetConnection client in server.Connections)
                                {
                                    NetOutgoingMessage msg = server.CreateMessage();
                                    msg.Write("PlayerDisconnect");
                                    msg.Write(connections[c.RemoteUniqueIdentifier]);
                                    server.SendMessage(msg,client, NetDeliveryMethod.ReliableOrdered);
                                }

                                connections.Remove(c.RemoteUniqueIdentifier);
                            }

                            break;
                        case NetIncomingMessageType.Data:
                            // The client sent input to the server
                            string type = message.ReadString();

                            if (type.Equals("PlayerMeta"))
                            {
                                float x = message.ReadFloat();
                                float y = message.ReadFloat();
                                float rA = message.ReadFloat();
                                float rX = message.ReadFloat();
                                float rY = message.ReadFloat();
                                message.SenderConnection.Tag = new float[] {
                                    x, y, rA, rX, rY
                                };
                            }
                            else if (type.Equals("BulletInfo"))
                            {

                            }
                            break;
                    }

                    //
                    // send position updates 30 times per second
                    //
                    double now = NetTime.Now;
                    if (now > nextMessageTime)
                    {
                        try
                        {
                            int numMessages = server.Connections.ToArray().Length * 3;

                            foreach (NetConnection c in server.Connections)
                            {
                                NetOutgoingMessage msg = server.CreateMessage();

                                msg.Write("Updates");
                                msg.Write(numMessages);
                                foreach (NetConnection conn in server.Connections)
                                {
                                    msg.Write(connections[conn.RemoteUniqueIdentifier]);
                                    float[] tag = conn.Tag as float[];
                                    msg.Write(tag[0]);
                                    msg.Write(tag[1]);
                                    msg.Write(tag[2]);
                                    msg.Write(tag[3]);
                                    msg.Write(tag[4]);
                                }
                                server.SendMessage(msg, c, NetDeliveryMethod.ReliableOrdered);
                            }

                            nextMessageTime += (1.0 / 30.0);
                        }
                        catch
                        {

                        }
                    }
                }

                // sleep to allow other processes to run smoothly
                Thread.Sleep(1);
            }

            server.Shutdown("app exiting");
        }
    }
}